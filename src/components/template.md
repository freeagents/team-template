Stateless components. Shouldn't store state. Most components in this directory will be function-based components. Stuff like buttons, inputs, and label components goes here. These components can also accept functions as props and dispatch events, but no state should be held inside.

Components should not leak margin. Parent containers will space components.